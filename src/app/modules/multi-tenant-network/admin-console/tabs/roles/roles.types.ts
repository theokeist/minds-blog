export enum MultiTenantRolesView {
  PERMISSIONS = 'permissions',
  USERS = 'users',
}

export enum RoleId {
  OWNER = 0,
  ADMIN = 1,
  MODERATOR = 2,
  VERIFIED = 3,
  DEFAULT = 4,
}
