import { Component } from '@angular/core';

/**
 * Network admin console moderation section. Contains sub-tabs
 * for sections involving moderation.
 */
@Component({
  selector: 'm-networkAdminConsole__moderation',
  templateUrl: './moderation.component.html',
  styleUrls: [
    './moderation.component.ng.scss',
    '../../stylesheets/console.component.ng.scss',
  ],
})
export class NetworkAdminConsoleModerationComponent {}
