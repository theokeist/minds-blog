export let siteServiceMock = new (function() {
  var pro = () => null;
  var isProDomain = () => false;
  var title = () => 'Minds';
  var isAdmin = () => true;
})();
