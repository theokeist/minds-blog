import { InjectionToken } from '@angular/core';

// Site name.
export const SITE_NAME = new InjectionToken<string>('site_name');

// Reference to window.
export const WINDOW = new InjectionToken<string>('window');
